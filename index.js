const express = require("express");
const path = require("path");
const app = express();
const users_app = require("./users.json");
const port = 3000;
const count_users = users_app;

app.use("/landing", express.static(path.resolve(__dirname, "landing")));
app.use(
  "/landing/gameplay",
  express.static(path.resolve(__dirname, "gameplay"))
);
app.use("/login", express.static(path.resolve(__dirname, "login")));
app.get("/data", (req, res) => {
  res.send(`jumlah user ${count_users.length}`);
});
app.get("/users", function (req, res) {
  res.send(users_app);
});

app.listen(port, () =>
  console.log(`server already running on : http://localhost:${port}/landing`)
);
